/* --------------start color plate transition----------- */
$(".icon").show();
$(".icon2").hide();
  
$(".icon").click(function(){
    $(".color-plate-wrapper").animate({
        width:'slideToggle',
        left:'-13px',
    },500);
    $(".icon").hide();
    $(".icon2").show();
}); 

$(".icon2").click(function(){
    $(".color-plate-wrapper").animate({
        width:'slideToggle',
        left:'-133px',
    },500);
    $(".icon2").hide();
    $(".icon").show();
});
/* --------------end color plate transition----------- */


$(document).ready(function(){

    $(".blue").click(function(){
        $(".blue-color").addClass("blue-color");
        $(".blue-background").addClass("blue-background");
        $(".blue-button-shadow").addClass("blue-button-shadow");
        $(".blue-transparent-background").addClass("blue-transparent-background");
		
        $(".blue-color").removeClass("green-color");
        $(".blue-background").removeClass("green-background");
        $(".blue-button-shadow").removeClass("green-button-shadow");
        $(".blue-transparent-background").removeClass("green-transparent-background");
		
        $(".blue-color").removeClass("purple-color");
        $(".blue-background").removeClass("purple-background");
        $(".blue-button-shadow").removeClass("purple-button-shadow");
        $(".blue-transparent-background").removeClass("purple-transparent-background");
		
        $(".blue-color").removeClass("red-color");
        $(".blue-background").removeClass("red-background");
        $(".blue-button-shadow").removeClass("red-button-shadow");
        $(".blue-transparent-background").removeClass("red-transparent-background");
		
        $(".blue-color").removeClass("yellow-color");
        $(".blue-background").removeClass("yellow-background");
        $(".blue-button-shadow").removeClass("yellow-button-shadow");
        $(".blue-transparent-background").removeClass("yellow-transparent-background");
		
        $(".blue-color").removeClass("aqua-color");
        $(".blue-background").removeClass("aqua-background");
        $(".blue-button-shadow").removeClass("aqua-button-shadow");
        $(".blue-transparent-background").removeClass("aqua-transparent-background");

    });
	
    $(".green").click(function(){
        $(".blue-color").addClass("green-color");
        $(".blue-background").addClass("green-background");
        $(".blue-button-shadow").addClass("green-button-shadow");
        $(".blue-transparent-background").addClass("green-transparent-background");
		
        $(".blue-color").removeClass("purple-color");
        $(".blue-background").removeClass("purple-background");
        $(".blue-button-shadow").removeClass("purple-button-shadow");
        $(".blue-transparent-background").removeClass("purple-transparent-background");
		
        $(".blue-color").removeClass("red-color");
        $(".blue-background").removeClass("red-background");
        $(".blue-button-shadow").removeClass("red-button-shadow");
        $(".blue-transparent-background").removeClass("red-transparent-background");
		
        $(".blue-color").removeClass("yellow-color");
        $(".blue-background").removeClass("yellow-background");
        $(".blue-button-shadow").removeClass("yellow-button-shadow");
        $(".blue-transparent-background").removeClass("yellow-transparent-background");
		
        $(".blue-color").removeClass("aqua-color");
        $(".blue-background").removeClass("aqua-background");
        $(".blue-button-shadow").removeClass("aqua-button-shadow");
        $(".blue-transparent-background").removeClass("aqua-transparent-background");
		
    });
	
    $(".purple").click(function(){
        $(".blue-color").addClass("purple-color");
        $(".blue-background").addClass("purple-background");
        $(".blue-button-shadow").addClass("purple-button-shadow");
        $(".blue-transparent-background").addClass("purple-transparent-background");

        $(".blue-color").removeClass("red-color");
        $(".blue-background").removeClass("red-background");
        $(".blue-button-shadow").removeClass("red-button-shadow");
        $(".blue-transparent-background").removeClass("red-transparent-background");
		
        $(".blue-color").removeClass("yellow-color");
        $(".blue-background").removeClass("yellow-background");
        $(".blue-button-shadow").removeClass("yellow-button-shadow");
        $(".blue-transparent-background").removeClass("yellow-transparent-background");
		
        $(".blue-color").removeClass("aqua-color");
        $(".blue-background").removeClass("aqua-background");
        $(".blue-button-shadow").removeClass("aqua-button-shadow");
        $(".blue-transparent-background").removeClass("aqua-transparent-background");
		
    });
	
    $(".red").click(function(){
        $(".blue-color").addClass("red-color");
        $(".blue-background").addClass("red-background");
        $(".blue-button-shadow").addClass("red-button-shadow");
        $(".blue-transparent-background").addClass("red-transparent-background");

        $(".blue-color").removeClass("yellow-color");
        $(".blue-background").removeClass("yellow-background");
        $(".blue-button-shadow").removeClass("yellow-button-shadow");
        $(".blue-transparent-background").removeClass("yellow-transparent-background");

        $(".blue-color").removeClass("aqua-color");
        $(".blue-background").removeClass("aqua-background");
        $(".blue-button-shadow").removeClass("aqua-button-shadow");
        $(".blue-transparent-background").removeClass("aqua-transparent-background");
		
    });
	
    $(".yellow").click(function(){
        $(".blue-color").addClass("yellow-color");
        $(".blue-background").addClass("yellow-background");
        $(".blue-button-shadow").addClass("yellow-button-shadow");
        $(".blue-transparent-background").addClass("yellow-transparent-background");
		
        $(".blue-color").removeClass("aqua-color");
        $(".blue-background").removeClass("aqua-background");
        $(".blue-button-shadow").removeClass("aqua-button-shadow");
        $(".blue-transparent-background").removeClass("aqua-transparent-background");

    });
	
    $(".aqua").click(function(){
        $(".blue-color").addClass("aqua-color");
        $(".blue-background").addClass("aqua-background");
        $(".blue-button-shadow").addClass("aqua-button-shadow");
        $(".blue-transparent-background").addClass("aqua-transparent-background");

    });



	
	
});  /* end document ready function */