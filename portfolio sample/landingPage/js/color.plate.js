/* -----------for color plate-------------- */
$(document).ready(function(){

    $(".color1").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg");
        $(".color-plate-color").addClass(".color-plate-color");
		
        $(".color-plate-bg").removeClass("color-plate-bg2");
        $(".color-plate-color").removeClass("color-plate-color2");
        $(".color-plate-bg").removeClass("color-plate-bg3");
        $(".color-plate-color").removeClass("color-plate-color3");
        $(".color-plate-bg").removeClass("color-plate-bg4");
        $(".color-plate-color").removeClass("color-plate-color4");
        $(".color-plate-bg").removeClass("color-plate-bg5");
        $(".color-plate-color").removeClass("color-plate-color5");
        $(".color-plate-bg").removeClass("color-plate-bg6");
        $(".color-plate-color").removeClass("color-plate-color6");
    });
	
    $(".color2").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg2");
        $(".color-plate-color").addClass("color-plate-color2");
		
        $(".color-plate-bg").removeClass("color-plate-bg3");
        $(".color-plate-color").removeClass("color-plate-color3");
        $(".color-plate-bg").removeClass("color-plate-bg4");
        $(".color-plate-color").removeClass("color-plate-color4");
        $(".color-plate-bg").removeClass("color-plate-bg5");
        $(".color-plate-color").removeClass("color-plate-color5");
        $(".color-plate-bg").removeClass("color-plate-bg6");
        $(".color-plate-color").removeClass("color-plate-color6");
		
    });
	
    $(".color3").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg3");
        $(".color-plate-color").addClass("color-plate-color3");
		
        $(".color-plate-bg").removeClass("color-plate-bg4");
        $(".color-plate-color").removeClass("color-plate-color4");
        $(".color-plate-bg").removeClass("color-plate-bg5");
        $(".color-plate-color").removeClass("color-plate-color5");
        $(".color-plate-bg").removeClass("color-plate-bg6");
        $(".color-plate-color").removeClass("color-plate-color6");
    });
	
    $(".color4").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg4");
        $(".color-plate-color").addClass("color-plate-color4");
		
        $(".color-plate-bg").removeClass("color-plate-bg5");
        $(".color-plate-color").removeClass("color-plate-color5");
        $(".color-plate-bg").removeClass("color-plate-bg6");
        $(".color-plate-color").removeClass("color-plate-color6");
		
    });
	
    $(".color5").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg5");
        $(".color-plate-color").addClass("color-plate-color5");
		
        $(".color-plate-bg").removeClass("color-plate-bg6");
        $(".color-plate-color").removeClass("color-plate-color6");
		
    });
	
    $(".color6").click(function(){
        $(".color-plate-bg").addClass("color-plate-bg6");
        $(".color-plate-color").addClass("color-plate-color6");
		
    });
    /* -----------end color plate-------------- */
	 
	 
	 
	 
    /* --------------start color plate transition----------- */
    $(".icon").show();
    $(".icon2").hide();
  
    $(".icon").click(function(){
        $(".color-plage-wrapper").animate({
            width:'slideToggle',
            left:'-13px',
        },500);
        $(".icon").hide();
        $(".icon2").show();
    }); 

    $(".icon2").click(function(){
        $(".color-plage-wrapper").animate({
            width:'slideToggle',
            left:'-241px',
        },500);
        $(".icon2").hide();
        $(".icon").show();
    });
/* --------------end color plate transition----------- */
	
	
	 
	 
	 
	
	
	
	
	
});/* end document ready function */