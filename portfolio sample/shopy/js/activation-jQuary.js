/* scrollUp Minimum setup */
$(function () {
   $.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '500', // Distance from top before showing element (px)
	topSpeed: 700, // Speed back to top (ms)
	animation: 'slide', // Fade, slide, none
	animationInSpeed: 700, // Animation in speed (ms)
	animationOutSpeed: 700, // Animation out speed (ms)
	scrollText: 'Scroll to top', // Text for element
	activeOverlay: true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
  
});


/* stick menu */
$(document).ready(function() {
    var s = $("#sticker");
    var pos = s.position();					   
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        if (windowpos >= pos.top) {
            s.addClass("stick");
            $('.slider-section').addClass('extra-margin');
            $('.all-products').addClass('extra-margin');
        } else {
            s.removeClass("stick");
            $('.slider-section').removeClass('extra-margin');			
            $('.all-products').removeClass('extra-margin');			
        }
    });
});


 
$(document).ready(function() {

    /* new product carousel for index page */
    $("#product-carousel").owlCarousel({
        items : 4,
        autoPlay: 3000, //Set AutoPlay to 3 seconds
        slideSpeed: 900,
        stopOnHover : true,
        paginationSpeed:900,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [991,2],
		itemsMobile:[580,2],
		itemsMobile:[480,1]
 
    });
  
  	
	
    /* slider js */
    jQuery('.tp-banner').show().revolution(
    {
        dottedOverlay:"none",
        delay:10000,
        startwidth:1170,
        startheight:600,

        navigationType:"bullet",
        navigationArrows:"solo",
        navigationStyle:"preview3",
						
        touchenabled:"on",
        onHoverStop:"off",
							
        shadow:0,
        fullWidth:"on",
        fullScreen:"off",

        spinner:"spinner4",
						
    });
/* end slider js */
  

 
});





